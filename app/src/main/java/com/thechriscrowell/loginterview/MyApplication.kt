package com.thechriscrowell.loginterview

import android.app.Application
import com.thechriscrowell.oak.DefaultLogger
import com.thechriscrowell.oak.Oak

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Oak.registerLogger(DefaultLogger())
    }
}