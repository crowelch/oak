package com.thechriscrowell.loginterview.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.thechriscrowell.loginterview.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewModel: MainActivityViewModel by viewModels()

        viewModel.sendInitAnalytic()

        log_button.setOnClickListener { viewModel.sendButtonLog(ButtonObject) }
    }
}
