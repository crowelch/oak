package com.thechriscrowell.loginterview.main

import android.util.Log
import androidx.lifecycle.ViewModel
import com.thechriscrowell.oak.Oak

class MainActivityViewModel : ViewModel() {

    fun sendInitAnalytic() {
        Oak.log(TestObject)
    }

    fun sendButtonLog(button: ButtonObject) {
        Oak.log(button, Log.WARN)
    }
}