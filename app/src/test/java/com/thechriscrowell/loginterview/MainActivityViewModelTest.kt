package com.thechriscrowell.loginterview

import com.thechriscrowell.loginterview.main.MainActivityViewModel
import com.thechriscrowell.loginterview.trees.TestTree
import io.mockk.MockKAnnotations
import io.mockk.mockk
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainActivityViewModelTest {

    private val mockTestTree: TestTree = mockk()

    private val mainActivityViewModel = MainActivityViewModel()

    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @After
    fun teardown() {
        unmockkAll()
    }


    @Test
    fun `when sendInitAnalytics is called, testTree log should be called`() {
        mainActivityViewModel.sendInitAnalytic()

        verify { mockTestTree.log(any(), "Main activity initialized. Welcome %s", Interview().interviewee) }
    }
}