package com.thechriscrowell.oak

interface OakLogger {
    fun log(toLog: Any, urgency: Int)
}
