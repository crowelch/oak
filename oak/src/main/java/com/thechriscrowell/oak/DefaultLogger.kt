package com.thechriscrowell.oak

import android.util.Log

class DefaultLogger : OakLogger {
    override fun log(toLog: Any, urgency: Int) {
        when (urgency) {
            Log.VERBOSE -> Log.v("OakLogger Rules", toLog.toString())
            Log.DEBUG -> Log.d("OakLogger Rules", toLog.toString())
            Log.INFO -> Log.i("OakLogger Rules", toLog.toString())
            Log.WARN -> Log.w("OakLogger Rules", toLog.toString())
            Log.ERROR -> Log.e("OakLogger Rules", toLog.toString())
            else -> {
                /** no-op? could be handled higher up **/
            }
        }
    }
}
