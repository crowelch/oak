package com.thechriscrowell.oak

import android.util.Log

class Oak {
    companion object {
        private val loggers: MutableList<OakLogger> = mutableListOf()

        fun log(
            toLog: Any?,
            urgency: Int = Log.VERBOSE
        ) { // could handle defaulting differently if targeting Java usage
            toLog?.takeUnless { it is String }?.let {
                logAll(it, urgency)
            } ?: run {
                throw Exception("No strings allowed")
            }
        }

        @JvmStatic
        private fun logAll(toLog: Any, urgency: Int) {
            loggers.forEach { it.log(toLog, urgency) }
        }

        @JvmStatic
        fun registerLogger(logger: OakLogger) {
            loggers.add(logger)
        }

        @JvmStatic
        fun unregisterLogger(logger: OakLogger) {
            loggers.remove(logger)
        }

        @JvmStatic
        fun unregisterAllLoggers() {
            loggers.clear()
        }
    }
}
